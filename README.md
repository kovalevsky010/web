# OS lab_2

**linux**

#include <fcntl.h>

int main() {
  int hstdin = 0;
  char* filename = "data.txt";
  char buffer[100];

  int fhandle = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0600);

  if (!fhandle) return -1;  

  int cb = read(hstdin, buffer, 100);

  write(fhandle, buffer, cb);

  close(fhandle);
}



#include <fcntl.h>
#include <stdio.h>

int main() {
  char* filename = "data.txt";
  char buffer[100];
  int hstdout = 1;

  int h1 = open(filename, O_RDONLY);
  int h2 = dup(h1);
  int h3 = open(filename, O_RDONLY);

  if (!h1 || !h2 || !h3) return -1;

  printf("h1: %d\nh2: %d\nh3: %d\n", h1, h2, h3);

  lseek(h1, 10, SEEK_SET);

  int cb = read(h1, buffer, 7);
  write(hstdout, buffer, cb);
  printf("\n");

  cb = read(h2, buffer, 7);
  write(hstdout, buffer, cb);
  printf("\n");

  cb = read(h3, buffer, 7);
  write(hstdout, buffer, cb);
  printf("\n");

  close(h1);
  close(h2);
  close(h3);
}

**windows**

#include <windows.h>
#include <string.h>

int main() {
  char* filename = "data.txt";
  char buffer[100];
  DWORD actlen;

  HANDLE hstdin = GetStdHandle(STD_INPUT_HANDLE);
  HANDLE fhandle = CreateFile(filename, GENERIC_WRITE, 0, 0,
		  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
  
  if (!hstdin || !fhandle) return -1;

  ReadFile(hstdin, buffer, 100, &actlen, NULL);

  WriteFile(fhandle, buffer, strlen(buffer), &actlen, NULL);

  CloseHandle(fhandle);
}



#include <windows.h>
#include <string.h>
#include <stdio.h>

int main() {
  char* filename = "data.txt";
  char buffer[100];
  DWORD actlen;
  HANDLE fh1, fh2, fh3;

  HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
  fh1 = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, 0,
                        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

  DuplicateHandle(GetCurrentProcess(), fh1, GetCurrentProcess(),
                            &fh2, 0, FALSE, DUPLICATE_SAME_ACCESS);

  fh3 = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, 0,
                        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

  printf("fh1: %d\nfh2: %d\nfh3: %d\n", fh1, fh2, fh3);

  if (!fh1 || !fh2 || !fh3) return -1;

  SetFilePointer(fh1, 10, 0, FILE_BEGIN);

  ReadFile(fh1, buffer, 7, &actlen, NULL);
  WriteFile(hstdout, buffer, strlen(buffer), &actlen, NULL);
  printf("\n");

  ReadFile(fh2, buffer, 7, &actlen, NULL);
  WriteFile(hstdout, buffer, strlen(buffer), &actlen, NULL);
  printf("\n");

  ReadFile(fh3, buffer, 7, &actlen, NULL);
  WriteFile(hstdout, buffer, strlen(buffer), &actlen, NULL);
  printf("\n");

  CloseHandle(fh1);
  CloseHandle(fh2);
  CloseHandle(fh3);
}
